# ConoHa v3に\\nDebianを\\nインストールしてみた

subtitle
:  ConoHa v3 + custom preseed.cfg = bookworm VPS!

author
:  Kentaro Hayashi

institution
:  ClearCode Inc.

content-source
:  2024年01月 東京エリア・関西合同Debian勉強会

allotted-time
:  20m

theme
:  .

# スライドは\\nRabbit Slide Showにて公開済み

* ConoHa v3にDebianをインストールしてみた
  * {::note}<https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-conoha-v3-debian-202401>{:/note}

# 元記事公開済み

* ConoHa VPS (v3)にDebianをインストールする方法(2024年1月時点)
  * <https://kenhys.hatenablog.jp/entry/2024/01/14/171628>

# 本日の内容

* ConoHa VPS (v3)でDebianをインストールしてみた話
  * ConoHaのインフラが刷新されv3が利用可能になった
  * ConoHa v3は標準ではDebianをサポートしていない
  * ConoHa v3では各種APIがサポートされている
  * カスタムイメージを使って再インストールを試みる

# ConoHaとは

![https://www.conoha.jp](images/conoha.png){:relative-height="60"}

* GMOグループ傘下のホスティングサービス
  * 今日の話題は**VPS**。{::note}VPSの時間課金での利用もできる 👍{:/note} 

# ConoHa v2とv3

* ❌ 管理画面で切り替えられるので、単なるコンパネのバージョンの違い?(と勘違い)
* ✅ サーバーインフラがv2とv3で別物
  * v2のインスタンスとv3のインスタンスは完全に別管理

# ConoHa v3の残念なところ

* ❌ Ubuntuは20.04と22.04が選べるが、Debianは選べない {::note}(2024/01/14現在){:/note}
  * v2のときはDebianも選択肢にあったのに。。。 
    * {::note}~~(OpenBSDやNetBSDも消えた)~~ 2024/01/10 提供再開のお知らせ{:/note}
  * FreeBSDやArchはv3でもイメージがサポートされている

# ~~ConoHa v3の残念なところ~~

> 標準でサポートされていないなら
  自分でインストールすればいいじゃない

# ConoHa v2のときの\\nカスタムインストール方法

* CLIツールで簡単にISOイメージをマウントする
  * <https://support.conoha.jp/v/clitools/>
    * v3はインフラが別なのでこの方法は使えない

# ConoHa v3のAPIでできないか?

* <https://doc.conoha.jp/api-vps3/>
  * 「APIでVPSにISOイメージを挿入する」ためのドキュメント
    * <https://doc.conoha.jp/api-vps3/api-mount_iso_image-v3/>

# v3 APIでVPSに\\nISOイメージを挿入する(1)

* インスタンスを作成後停止しておく
* トークンを作成 (APIアクセスに必要)
* イメージIDを作成 (あとでISOをアップロードするときに使う)
* ISOイメージ(bookworm netinst .iso)をアップロード

# v3 APIでVPSに\\nISOイメージを挿入する(2)

* 停止済みインスタンスにISOをマウント
* レスキューモードでISOイメージからインストーラーを起動
* コンソールURIを発行して操作

# Debian公式ISOイメージを\\n利用した場合の課題

* ❌セキュアなインスタンスの初期構築が手間
  * 標準イメージのように、あらかじめ.ssh/authorized_keysを仕込めない
* ❌ConoHa標準のVNCコンソールがちょっと不便
    * テキスト送信や特殊キー送信経由では`|`や`_`などの記号が打てず欠落する
    * vimで直接いじるにも操作感が通常と異なる (要検証)

# カスタムISOイメージによる\\n再インストールで解決

* ✅メリット
  * あらかじめ必要な設定を仕込んだ状態でインストールできる
    * 自動化はpreseed.cfgを使えばよさそう
    * あるいはFAI(Fully Automatic Installer) or Simple-CDD?
* ❌デメリット
  * カスタムイメージ作成のノウハウが必要

# 今回はpreseed.cfgを使ってみた

* 参考: 2016年10月のDebian勉強会資料
  * {::note}<https://tokyodebian-team.pages.debian.net/pdf2016/debianmeetingresume201610-presentation-sugimoto.pdf>{:/note}

# preseed.cfgの仕込み方

* initrd.gzに仕込む (CDイメージ再生成必要)
* 起動時パラメーターでpreseed.cfgを参照させる
  * file=...でCDイメージに含めたpreseed.cfgを参照する (CDイメージ再生成必要)
  * url=でネットワーク上のpreseed.cfgを参照させる

* {::note}VNCコンソールへの切り替え中に誤爆しないようにpreseed.cfgをCDイメージに含める{:/note}

# カスタムISOへの道

* isolinux/menu.cfg
  * 搭載メモリが少ないインスタンスではGUIインストールできない
    * Graphical Installを削除を推奨 (gtk.cfgをコメントアウト)
* isolinux/txt.cfg
  * テキストモードをデフォルトにする
  * file=/cdrom/preseed/preseed.cfgを参照させる
  * 言語やロケール、キーマップを指定する

# isolinux/menu.cfg

```
menu title Debian GNU/Linux installer menu (BIOS mode)
include stdmenu.cfg
#include gtk.cfg
include txt.cfg
menu begin advanced
...
```

* グラフィカルインストールの選択肢をコメントアウトして殺す

# isolinux/txt.cfg

```
label install
       menu label ^Install
       menu default
       kernel /install.amd/vmlinuz
       append language=en country=US keymap=us \
         file=/cdrom/preseed/preseed.cfg vga=788 initrd=/install.amd/initrd.gz --- quiet 
```

* file=でpreceed.cfgを指定、テキストモードを既定(menu default)にしておく

# preseed.cfgで公開鍵を仕込む

```
# Setup public key
d-i preseed/late_command string \
  cp /cdrom/preseed/sshd_config.d.local /target/etc/ssh/sshd_config.d/local.conf ; \
  mkdir -p /target/home/debian/.ssh ; \
  cp /cdrom/preseed/authorized_keys /target/home/debian/.ssh/ ; \
  in-target chown debian:debian -R /home/debian/.ssh ; \
  in-target chmod 400 /home/debian/.ssh/authorized_keys
```

* preseed/late_commandでファイルを配置する
  * {::note}in-targetかどうかを意識しないとハマる{:/note}

# リマスタリングISO

```
(cd custom-iso; \
  find -follow -type f ! -name md5sum.txt -print0 | xargs -0 md5sum > md5sum.txt)
sudo genisoimage -quiet -r -J -b isolinux/isolinux.bin \
  -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table \
  -o netinst.iso custom-iso
```

* CDに追加でファイルを仕込むので、チェックサムを更新して再作成する

# 再インストール実演

* 前提
  * 1 core/512MBメモリの最小構成
    * {::note}ConoHa VPSではUbuntu 20.04でインスタンス作成時に最小構成を選択できる{:/note}
  * Debian bookworm 12.4.0を採用
  * .ssh/authorized_keyを仕込み公開鍵認証のみ
    * {::note}デモなのでネットワーク設定まではがんばらない{:/note}
  
# さいごに

* ConoHa VPS (v3)でDebianが標準イメージとして提供されるようになるまでのつなぎの技術を紹介しました。
* カスタムISOイメージを使ってインストールする手段あればなんとかなる
  * **~~ConoHa VPS (v3)でDebianの標準イメージが提供されるのも\\nそう遠くない未来だろうから試すなら今~~**
