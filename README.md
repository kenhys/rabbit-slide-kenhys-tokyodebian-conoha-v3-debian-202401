# Tokyo Debian ConoHa v3 + Debian bookworm + preceed.cfg

## How to show

  % rabbit

## How to install

  % gem install rabbit-slide-kenhys-tokyodebian-conoha-v3-debian-202401

## How to create PDF

  % rake pdf


# References

* ConoHa VPS 3.0 APIドキュメント

  https://doc.conoha.jp/api-vps3/

* https://wiki.debian.org/DebianInstaller/Preseed/EditIso

* ConoHa v3に\\nDebianを\\nインストールしてみた
 
  https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-conoha-v3-debian-202401

* ConoHa VPS (v3)にDebianをインストールする方法(2024年1月時点)

  https://kenhys.hatenablog.jp/entry/2024/01/14/171628

